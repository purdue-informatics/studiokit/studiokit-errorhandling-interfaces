﻿using System;

namespace StudioKit.ErrorHandling.Interfaces;

public interface IErrorHandler
{
	void CaptureException(Exception exception);

	void CaptureException(Exception exception, IExceptionUser user);

	void CaptureException(Exception exception, IExceptionUser user, object extra);
}