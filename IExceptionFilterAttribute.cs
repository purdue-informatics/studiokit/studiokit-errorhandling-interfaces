﻿using System;

namespace StudioKit.ErrorHandling.Interfaces;

public interface IExceptionFilterAttribute
{
	bool IsExceptionIgnored(Exception exception);
}