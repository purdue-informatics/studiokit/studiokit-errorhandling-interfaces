﻿namespace StudioKit.ErrorHandling.Interfaces;

public interface IExceptionUser
{
	string Id { get; set; }

	string UserName { get; set; }

	string Email { get; set; }
}